// import axios from 'axios';

export const state = () => ({
    list:[
        {
            label:'Birthday Words &ndash; Oxford University Press',
            url:'/projects/oed',
            desc:'CSS made 3D Cubes reveal a word intrudcued to the dictionary the year you were born',
        },
        {
            label:'LEXI 2014 Paralympic Guidelines &ndash; Channel&nbsp;4',
            url:'/projects/lexi2014',
            desc:'Interactive guide to Sochi 2014 Paralympic Classifications'
        },
        {
            label:'Geimfar',
            url:'/projects/geimfar',
            desc:'DIY identity for a creative space'
        },
        {
            label:'SVG Morse Generator',
            url:'/projects/svg-morse-generator',
            desc:'A brief story about my business cards',
        },
        {
            label:'AdPlanner &ndash; Yahoo! EMEA Research and Insights',
            url:'/projects/ad-planner',
            desc:'Advertsing research visualised'
        },
        {
            label:'Independent Voices &ndash; The Independent',
            url:'/projects/voices',
            desc:'Design system for The Independent\'s new platform'
        },
        {
            label:'Lime theatre &ndash; Spektrix',
            url:'/projects/spektrix',
            desc:'Spektrix API demo responsive website'
        },
        {
            label:'Typemix',
            url:'/projects/typemix',
            desc:'Adventures in random typography'
        },

    ]
});


export const getters = {
  getProjects: (state) => () => {
    return state.projects;
  }
}
